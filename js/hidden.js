document.addEventListener("DOMContentLoaded", function() {
    // Определение всех элементов с атрибутом hidden
    const hiddenElements = document.querySelectorAll('[hidden]');

    // Скрытие элементов при первой загрузке
    hiddenElements.forEach(element => {
        element.style.display = 'none';
    });

    // Отложенное отображение элементов через 10 секунд
    setTimeout(() => {
        hiddenElements.forEach(element => {
            element.style.display = '';
        });
    }, 5000);
});
